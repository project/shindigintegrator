
/**
*
* some code of this file is referenced from Partuza. 
* http://code.google.com/p/partuza/source/browse/trunk/html/js/container.js
* By Chris Chabot
*
*/

if (Drupal.jsEnabled) {
  var maxHeight = 4096;
  var gadgets = gadgets || {};
  
  /**
  *  Base implementation of Gadget Service
  *  registering functions
  */
  gadgets.IfrContainer = function() {
    gadgets.rpc.register('resize_iframe', this.setHeight);
    gadgets.rpc.register('requestNavigateTo', this.requestNavigateTo);
  };

  /**
  *  function to adjust height of the gadget on the fly
  */
  gadgets.IfrContainer.prototype.setHeight = function(height) {
    if ($(this.f) != undefined) {
  	  height += 28;
  	  if (height > maxHeight) {
        height = maxHeight;
	  }
	  $(this.f).setAttribute('height',height+'px');
    }  
  };
  
  /**
  *  function to Parse Iframe url to get the parameters in key=>value pair
  */
  gadgets.IfrContainer.prototype.parseIframeUrl = function(url) {
    // parse the iframe url to extract the key = value pairs from it
    var ret = new Object();
    var hashParams = url.replace(/#.*$/, '').split('&');
    var param = key = val = '';
    for (i = 0 ; i < hashParams.length; i++) {
      param = hashParams[i];
      key = param.substr(0, param.indexOf('='));
      val = param.substr(param.indexOf('=') + 1);
      ret[key] = val;
    }
    return ret;
  };
  
  /**
  *  function to get urls for different views
  */
  gadgets.IfrContainer.prototype.getUrlForView = function(view, person, app, module) {
    if (view === 'canvas') {
      return '/application_canvas/' + app + '/' + module;
    } else if (view === 'profile') {
      return '/user/' + person;
    } else {
      return null;
    }
  };
  
  /**
  * Navigates the page to a new url based on a gadgets requested view and
  * parameters.
  */
  gadgets.IfrContainer.prototype.requestNavigateTo = function(view, opt_params) {
    if ($(this.f) != undefined) {
      var params = gadgets.container.parseIframeUrl($(this.f).src);
      var url = gadgets.container.getUrlForView(view, params.owner, params.aid, params.mid);
      if (opt_params) {
        var paramStr = Object.toJSON(opt_params);
        if (paramStr.length > 0) {
	      url += '?appParams=' + encodeURIComponent(paramStr);
	    }
	  }
      if (url && document.location.href.indexOf(url) == -1) {
        document.location.href = url;
      }
    }
  };

  gadgets.container = new gadgets.IfrContainer();
  
 /**
 * function to show or hide loading image on iframe
 */
  function showIframe(iframe_id, load_id) {
    $(iframe_id).show();
    $(load_id).hide();
  }
}